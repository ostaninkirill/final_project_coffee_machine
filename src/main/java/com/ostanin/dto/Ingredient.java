package com.ostanin.dto;

import java.util.List;

public class Ingredient {
    private int id;
    private String name;
    private String unit;
    private int quantityOstatok;
    private List<Ingredient> ingredients;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getQuantityOstatok() {
        return quantityOstatok;
    }

    public void setQuantityOstatok(int quantityOstatok) {
        this.quantityOstatok = quantityOstatok;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
