package com.ostanin.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Objects;


public class User {

    @NotEmpty(message="Имя должно быть заполненено")
    private String name;
    @NotEmpty(message="Поле фамилия должно быть заполненено")
    private String surname;
    private Role role;
    @NotEmpty(message="Необходимо заполнить поле почты")
    @Email(message="Введите корректный адрес почты")
    private String email;
    @NotEmpty(message="Необходимо заполнить поле пароля")
    @Size(min = 5, message="Пароль должен быть длиннее 5")
    private String password;
    private int balance;
    private Order order;



    public User() {
        super();
        // TODO Auto-generated constructor stub
    }


    public User(@NotEmpty(message = "Имя должно быть заполнено") String name,
                @NotEmpty(message = "Фамилия должно быть заполнено") String surname,
                Role role,
                @NotEmpty(message = "Необходимо заполнить поле почты") @Email(message = "Введите корректный адрес почты") String email,
                @NotEmpty(message = "Необходимо заполнить поле пароля") @Size(min = 5, message = "Пароль должен быть длиннее 5") String password,
                int balance) {
        this.name = name;
        this.surname = surname;
        this.role = role;
        this.email = email;
        this.password = password;
        this.balance = balance;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Enum getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }


    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return balance == user.balance &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(role, user.role) &&
                Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) &&
                Objects.equals(order, user.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, role, email, password, balance, order);
    }
}


