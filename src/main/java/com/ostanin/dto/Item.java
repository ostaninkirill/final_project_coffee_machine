package com.ostanin.dto;


import java.util.List;

public class Item {
    private int id;
    private String title;
    private int price;
    private String weight;
    private String photo_link;
    private int idcategory;
    private List<Item> coffeeList;
    private List<Item> teaList;
    private List<Item> drinksList;


    public Item() {
        super();
        // TODO Auto-generated constructor stub
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getPhoto_link() {
        return photo_link;
    }

    public void setPhoto_link(String photo_link) {
        this.photo_link = photo_link;
    }

    public int getIdcategory() {
        return idcategory;
    }

    public void setIdcategory(int idcategory) {
        this.idcategory = idcategory;
    }


    public List<Item> getCoffeeList() {
        return coffeeList;
    }

    public void setCoffeeList(List<Item> coffeeList) {
        this.coffeeList = coffeeList;
    }

    public List<Item> getTeaList() {
        return teaList;
    }

    public void setTeaList(List<Item> teaList) {
        this.teaList = teaList;
    }

    public List<Item> getDrinksList() {
        return drinksList;
    }

    public void setDrinksList(List<Item> drinksList) {
        this.drinksList = drinksList;
    }
}
