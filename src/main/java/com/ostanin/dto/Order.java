package com.ostanin.dto;

public class Order {
    private String[] title;
    private int price1;
    private int kolichestvo;
    private Integer[] sugar;
    private String[] additional_ingredients;

    public Order(String[] title, Integer[] sugarList, String[] add_ingredients) {
        this.title = title;
        this.sugar = sugarList;
        this.additional_ingredients = add_ingredients;
    }


    public Order(String[] title, Integer[] sugarList) {
        this.title = title;
        this.sugar = sugarList;
    }

    public String[] getTitle() {
        return title;
    }

    public void setTitle(String[] title) {
        this.title = title;
    }

    public double getPrice1() {
        return price1;
    }

    public void setPrice1(int price1) {
        this.price1 = price1;
    }

    public int getKolichestvo() {
        return kolichestvo;
    }

    public void setKolichestvo(int kolichestvo) {
        this.kolichestvo = kolichestvo;
    }


    public Integer[] getSugar() {
        return sugar;
    }

    public void setSugar(Integer[] sugar) {
        this.sugar = sugar;
    }

    public String[] getAdditional_ingredients() {
        return additional_ingredients;
    }

    public void setAdditional_ingredients(String[] additional_ingredients) {
        this.additional_ingredients = additional_ingredients;
    }
}
